package instanciadores.pe;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import instanciadores.pe.adapters.DrawDividerItem;
import instanciadores.pe.adapters.PokemonAdapter;
import instanciadores.pe.models.entities.Pokemon;

public class GrillaActivity extends AppCompatActivity {

    ArrayList<Pokemon> pokemonList;
    RecyclerView mRecyclerViewListPokemon;
    PokemonAdapter mPokemonAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grilla);

        setRecyclerViewListPokemon();

       // Intent i = this.getIntent();
       // ArrayList<Pokemon> carList = i.getParcelableArrayListExtra(Constante.GRILLA_POKEMON);
    }

    private void setRecyclerViewListPokemon(){
        Intent i = this.getIntent();

        pokemonList = i.getParcelableArrayListExtra(Constante.GRILLA_POKEMON);

        mPokemonAdapter = new PokemonAdapter(this, pokemonList);

        mRecyclerViewListPokemon = (RecyclerView)findViewById(R.id.rvListaPokemon);
        mRecyclerViewListPokemon.setAdapter(mPokemonAdapter);
        mRecyclerViewListPokemon.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewListPokemon.addItemDecoration(new DrawDividerItem(this, LinearLayoutManager.VERTICAL));
        mRecyclerViewListPokemon.setLayoutManager(new LinearLayoutManager(this));

    }
}
