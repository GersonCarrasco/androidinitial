package instanciadores.pe.services;

import android.net.IpPrefix;

import instanciadores.pe.Constante;
import retrofit.RestAdapter;

/**
 * Created by PEPE on 10/06/2016.
 */
public class PokemonServiceImp {

private static RestAdapter restAdapter = new RestAdapter.Builder()
                                             .setEndpoint(Constante.URL)
                                             .build();

    public static IPokemonService getPokemonService(){
        return restAdapter.create(IPokemonService.class);
    }

}
