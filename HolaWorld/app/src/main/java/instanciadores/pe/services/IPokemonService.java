package instanciadores.pe.services;

import java.util.ArrayList;

import instanciadores.pe.models.entities.Pokemon;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by PEPE on 10/06/2016.
 */
public interface IPokemonService {
    @GET("/lista_pokemons.php")
    void getPokemon(Callback<ArrayList<Pokemon>> callback);

}
