package instanciadores.pe.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import instanciadores.pe.R;
import instanciadores.pe.models.entities.Pokemon;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

/**
 * Created by PEPE on 10/06/2016.
 */
public class PokemonAdapter extends RecyclerView.Adapter<PokemonViewHolder> {
    ArrayList<Pokemon> mPokemonArrayList;
    Context mContext;
    public PokemonAdapter(Context pContext,ArrayList<Pokemon> pPokemonArrayList){
        this.mContext = pContext;
        this.mPokemonArrayList = pPokemonArrayList;
    }
    @Override
    public PokemonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pokemon_list_row,parent,false);
        PokemonViewHolder viewHolder = new PokemonViewHolder(mContext,itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PokemonViewHolder holder, int position) {
        Pokemon objPokemon = mPokemonArrayList.get(position);
        holder.mtxtNombre.setText("Pokemon: " + objPokemon.getNombre().toString());
        holder.mtxtTipo.setText("Tipo: " + objPokemon.getTipo().toString());
        new DowloadImagenTask(holder.mImgPokemon).execute(objPokemon.getImagen().toString());
    }

    @Override
    public int getItemCount() {
        return mPokemonArrayList.size();
    }

    private class DowloadImagenTask extends AsyncTask<String,Void,Bitmap> {

        private ImageView mImageView;
        public DowloadImagenTask(ImageView pImageView){
            this.mImageView = pImageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String strUrlImage = params[0];
            Bitmap mBitmaplogo = null;

            try {
                InputStream inputs = new URL(strUrlImage).openStream();
                mBitmaplogo = BitmapFactory.decodeStream(inputs);
            } catch (IOException e) {
                Log.e("PokemonAdapter",e.toString());
            }
            return mBitmaplogo;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            mImageView.setImageBitmap(bitmap);
        }
    }

}
