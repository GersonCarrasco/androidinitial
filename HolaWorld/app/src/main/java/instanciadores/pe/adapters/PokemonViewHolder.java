package instanciadores.pe.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import instanciadores.pe.R;

/**
 * Created by PEPE on 10/06/2016.
 */
public class PokemonViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    public TextView mtxtNombre;
    public TextView mtxtTipo;
    public  ImageView mImgPokemon;

    public PokemonViewHolder(Context pContext,View pView){
        super(pView);
        this.mContext = pContext;
        this.mImgPokemon = (ImageView) pView.findViewById(R.id.imgPokemon);
        this.mtxtNombre = (TextView)pView.findViewById(R.id.txtPokemonNombre);
        this.mtxtTipo = (TextView)pView.findViewById(R.id.txtPokemonTipo);
    }
}
