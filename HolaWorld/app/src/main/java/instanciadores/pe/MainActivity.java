package instanciadores.pe;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import instanciadores.pe.models.entities.Pokemon;
import instanciadores.pe.services.PokemonServiceImp;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private EditText minputUser=null;
    private EditText minputPass=null;
    private Button mbtnIngresar = null;
    private ArrayList<Pokemon> lstPokemons = null;
    private Boolean isExist = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        minputUser = (EditText)findViewById(R.id.inputUser);
        minputPass = (EditText)findViewById(R.id.inputPassword);
        mbtnIngresar = (Button)findViewById(R.id.btnIngresar);

        PokemonServiceImp.getPokemonService().getPokemon(new Callback<ArrayList<Pokemon>>() {
            @Override
            public void success(ArrayList<Pokemon> pokemons, Response response) {
                lstPokemons = pokemons;
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("Paso algo",retrofitError.toString());
            }
        });


        mbtnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    for (Pokemon p: lstPokemons)
                    {
                        if (minputUser.getText().toString().equals(p.getNombre()) && minputPass.getText().toString().equals(p.getTipo())) {
                            isExist = true;
                            break;
                        }
                    }

                    if (isExist) {
                        Intent intentGrilla = new Intent(getApplication().getApplicationContext(),GrillaActivity.class);
                        intentGrilla.putParcelableArrayListExtra(Constante.GRILLA_POKEMON,lstPokemons);
                        startActivity(intentGrilla);
                    } else {
                        Toast.makeText(getApplication().getApplicationContext(), "El Pokemon no existe", Toast.LENGTH_SHORT).show();
                    }
                }
        });
    }

}
