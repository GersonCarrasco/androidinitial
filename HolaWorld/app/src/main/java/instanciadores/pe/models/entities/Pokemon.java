package instanciadores.pe.models.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by PEPE on 10/06/2016.
 */
public class Pokemon implements Parcelable {

    //region Variables
    public int id;
    public String nombre;
    public String tipo;
    public String imagen;
    //endregion

    //region Constructors
    protected Pokemon(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
        tipo = in.readString();
        imagen = in.readString();
    }
    //endregion

    //region Methods Parcelable
    public static final Creator<Pokemon> CREATOR = new Creator<Pokemon>() {
        @Override
        public Pokemon createFromParcel(Parcel in) {
            return new Pokemon(in);
        }

        @Override
        public Pokemon[] newArray(int size) {
            return new Pokemon[size];
        }
    };

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nombre);
        dest.writeString(tipo);
        dest.writeString(imagen);
    }

    //endregion

    //region Properties
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //endregion



}
